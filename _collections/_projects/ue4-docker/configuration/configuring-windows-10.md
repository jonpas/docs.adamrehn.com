---
title:  Configuring Windows 10
pagenum: 4
---

{% capture _alert_content %}
Windows 10 provides a sub-optimal experience when running both Windows and Linux containers, due to the following factors:

- Windows containers use [Hyper-V isolation mode](https://docs.microsoft.com/en-us/virtualization/windowscontainers/manage-containers/hyperv-container#hyper-v-isolation) by default, which suffers from [several issues that impact performance and stability](../read-these-first/windows-container-primer). Starting in Windows 10 version 1809 it is possible to [enable process isolation mode](https://docs.microsoft.com/en-us/virtualization/windowscontainers/about/faq#can-i-run-windows-containers-in-process-isolated-mode-on-windows-10), but this must be done manually.
- Linux containers are unable to use GPU acceleration via the [NVIDIA Container Toolkit](../read-these-first/nvidia-docker-primer).
{% endcapture %}
{% include alerts/warning.html content=_alert_content %}


## Requirements

- 64-bit Windows 10 Pro, Enterprise, or Education (Version 1607 or newer)
- Hardware-accelerated virtualisation enabled in the system BIOS/EFI
- Minimum 8GB of RAM
- Minimum {{ site.data.ue4-docker.common.diskspace_windows | escape }} available disk space for building container images


## Step 1: Install Docker CE for Windows

Download and install [Docker CE for Windows from the Docker Store](https://store.docker.com/editions/community/docker-ce-desktop-windows).


## Step 2: Install Python 3 via Chocolatey

The simplest way to install Python and pip under Windows is to use the [Chocolatey package manager](https://chocolatey.org/). To do so, run the following command from an elevated PowerShell prompt:

{% highlight powershell %}
Set-ExecutionPolicy Bypass -Scope Process -Force;
iex ((New-Object System.Net.WebClient).DownloadString(
	'https://chocolatey.org/install.ps1'
))
{% endhighlight %}

You may need to restart your shell for it to recognise the updates that the Chocolatey installer makes to the system `PATH` environment variable. Once these changes are recognised, you can install Python by running the following command from either an elevated PowerShell prompt or an elevated Command Prompt:

{% highlight powershell %}
choco install -y python
{% endhighlight %}


## Step 3: Install ue4-docker

Install the ue4-docker Python package by running the following command from an elevated Command Prompt:

{% highlight console %}
pip install ue4-docker
{% endhighlight %}


## Step 4: Manually configure Docker daemon settings

For building and running Windows containers:

- Configure the Docker daemon to [use Windows containers](https://docs.docker.com/docker-for-windows/#switch-between-windows-and-linux-containers) rather than Linux containers
- Configure the Docker daemon to increase the maximum container disk size from the default 20GB limit by following [the instructions provided by Microsoft](https://docs.microsoft.com/en-us/virtualization/windowscontainers/manage-containers/container-storage#storage-limits). The 120GB limit specified in the instructions is not quite enough, so set a 400GB limit instead. **Be sure to restart the Docker daemon after applying the changes to ensure they take effect.**

For building and running Linux containers:

- Configure the Docker daemon to [use Linux containers](https://docs.docker.com/docker-for-windows/#switch-between-windows-and-linux-containers) rather than Windows containers
- Use [the Advanced tab of the Docker settings pane](https://docs.docker.com/docker-for-windows/#advanced) to set the memory allocation for the Moby VM to 8GB and the maximum VM disk image size to {{ site.data.ue4-docker.common.diskspace_linux | escape }}.
