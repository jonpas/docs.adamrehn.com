---
title:  Microservices
blurb: >
  Thanks to the inclusion of conan-ue4cli infrastructure, the ue4-full
  image makes it easy to build UE4-powered microservices with Google's
  popular gRPC framework.
pagenum: 3
---

{% include alerts/info.html content="This page has migrated to the [Unreal Containers community hub](https://unrealcontainers.com/). You can find the new version here: [Use Cases: Microservices](https://unrealcontainers.com/docs/use-cases/microservices)." %}
