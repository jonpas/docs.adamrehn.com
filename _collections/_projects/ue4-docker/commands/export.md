---
title:  ue4-docker export
blurb: Exports components from built container images to the host system.
pagenum: 5
---

{{ page.blurb | escape }}

**Usage syntax:**

{% highlight bash %}
ue4-docker export COMPONENT TAG DESTINATION
{% endhighlight %}

This command can be used to export the following components:

* TOC
{:toc}


## Exporting Installed Builds under Linux

Installed Builds of UE4 can be exported to the host system starting with version 4.21.0. Once you have built the [ue4-full](../building-images/available-container-images#ue4-full) image for the UE4 version that you want to export, you can export it to the host system like so:

{% highlight bash %}
ue4-docker export installed 4.21.0 ~/UnrealInstalled
{% endhighlight %}

This will export the Installed Build of the Engine from the image `adamrehn/ue4-full:4.21.0` to the directory `~/UnrealInstalled` on the host system.


## Exporting Conan packages

The Conan wrapper packages generated by [conan-ue4cli]({{ site.data.common.projects.conan-ue4cli.repo }}) can be exported from the [ue4-full](../building-images/available-container-images#ue4-full) image to the local Conan package cache on the host system like so:

{% highlight bash %}
ue4-docker export packages 4.21.0 cache
{% endhighlight %}

[Conan](https://conan.io/) will need to be installed on the host system for this to work. To use the exported packages for development on the host system, you will also need to generate the accompanying profile-wide packages by running the command:

{% highlight bash %}
ue4 conan generate --profile-only
{% endhighlight %}

This will require both [ue4cli]({{ site.data.common.projects.ue4cli.repo }}) and [conan-ue4cli]({{ site.data.common.projects.conan-ue4cli.repo }}) to be installed on the host system.
