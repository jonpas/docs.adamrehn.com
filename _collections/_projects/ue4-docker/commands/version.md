---
title:  ue4-docker version
blurb: Prints the ue4-docker version number.
pagenum: 9
---

{{ page.blurb | escape }}

**Usage syntax:**

{% highlight bash %}
ue4-docker version
{% endhighlight %}
