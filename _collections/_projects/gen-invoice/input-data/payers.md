---
title:  Payers
pagenum: 3
---

## Description

Payer information represents the details of the entity that is receiving an invoice or quote. Unlike [line item](./items) data, payer data is completely freeform and is passed unmodified to [templates](./templates) by the invoice generator. This means the information schema for payer data is prescribed entirely by the template, providing users with the freedom to incorporate any type of payer information they like when writing templates.

Unlike [payee](./payees) data, there are no special circumstances under which the invoice generator will inspect the contents of payer data and transform it.


## Data format

{% include
	gen-invoice/formats-table.html
	cli="YAML file"
	api="[Mapping](https://docs.python.org/3/glossary.html#term-mapping)[[str](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str), [Any](https://docs.python.org/3/library/typing.html#typing.Any)]"
	rpc="*(coming soon)*"
%}


## Examples

**Note: the examples provided below follow the information schema prescribed by the default template that ships with the command-line interface for gen-invoice. Your own payer data may look very different if your template prescribes a different schema.**

Example YAML file for use with the command-line interface:

{% highlight yaml %}
name: XYZ Widget Company
address:
  - 1 Widget Road
  - Widgetville
  - WID 9999

due: Within 30 days of receipt
{% endhighlight %}

Example data for use with the API:

{% highlight json %}
{
  "name": "XYZ Widget Company",
  "address": [
    "1 Widget Road",
    "Widgetville",
    "WID 9999"
  ],
  "due": "Within 30 days of receipt"
}
{% endhighlight %}
