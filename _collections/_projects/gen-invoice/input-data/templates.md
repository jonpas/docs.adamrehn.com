---
title:  Templates
pagenum: 5
---

## Description

{% include gen-invoice/templates-overview.md %}

**For a full guide on writing templates, see the [Writing templates](../overview/writing-templates) page.**


## Data format

{% include
	gen-invoice/formats-table.html
	cli="HTML file"
	api="[str](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str) (string containing HTML code)"
	rpc="*(coming soon)*"
%}


## Examples

You can find the default template that ships with the command-line interface for gen-invoice here: <https://github.com/adamrehn/gen-invoice/blob/master/gen_invoice/defaults/default.html>

The default template is designed to be as generic and flexible as possible, serving as a potential basis for other templates. The default template is also designed for visual customisation via [stylesheets](./stylesheets), including the default stylesheet that ships with the command-line interface for gen-invoice.
