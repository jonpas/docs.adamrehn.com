---
title: ue4 conan boilerplate
blurb: Generates Unreal Engine modules with boilerplate code for encapsulating external dependencies.
layout: command
pagenum: 3
---

{% highlight bash %}
ue4 conan boilerplate [-outdir DIR] NAME
{% endhighlight %}

This command generates [boilerplate modules](../read-these-first/concepts#boilerplate-modules), which are simply standard Unreal Engine [External Modules](https://docs.unrealengine.com/en-US/Programming/BuildTools/UnrealBuildTool/ThirdPartyLibraries/index.html#modulesetup) with the addition of a [conanfile.py](https://docs.conan.io/en/latest/mastering/conanfile_py.html) for listing the Conan packages that they encapsulate and boilerplate code in their `.Build.cs` module rules file for querying Conan to resolve the configuration data required to link against the binaries from those Conan packages. The generated boilerplate code also includes logic for consuming [precomputed dependency data](../read-these-first/concepts#precomputed-dependency-data) created by the [ue4 conan precompute](./precompute) command if it is present. **See the [Consuming Conan packages](../workflow/consuming-packages) page for more details on generating boilerplate modules and populating the list of dependencies in their corresponding conanfile.py.**

The `NAME` argument specifies the name for the generated module, which will be automatically stripped of any disallowed characters to ensure it represents a [valid C# identifier](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/identifier-names). This sanitised identifier will be used as the name of the generated module directory, the prefix of the generated `.Build.cs` module rules filename, and the name of the class in the rules file.

By default, the module directory will be generated as a subdirectory of the current working directory. You can specify a different parent directory using the `-outdir DIR` argument.
