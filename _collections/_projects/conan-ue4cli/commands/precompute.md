---
title: ue4 conan precompute
blurb: Generates precomputed dependency data for Unreal Engine boilerplate modules.
layout: command
pagenum: 6
---

{% highlight bash %}
ue4 conan precompute PROFILE
{% endhighlight %}

This command generates [precomputed dependency data](../read-these-first/concepts#precomputed-dependency-data) for the dependencies of the [boilerplate module](../read-these-first/concepts#boilerplate-modules) in the current working directory, using the specified [UE4 Conan profile](../read-these-first/concepts#ue4-conan-profiles). **See the [Distributing projects and plugins](../workflow/distribution) page for more details on generating and distributing precomputed dependency data.**

The `PROFILE` argument specifies the UE4 Conan profile that should be used to generate precomputed dependency data. You can specify the value `host` to use the default profile for the host platform and the Unreal Engine version reported by [ue4cli](../../ue4cli).
