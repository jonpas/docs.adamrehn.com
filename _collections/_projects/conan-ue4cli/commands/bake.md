---
title: ue4 conan bake
blurb: Bake is a short alias for the precompute command.
pagenum: 2
---

{{ page.blurb | escape }} **See the [ue4 conan precompute](./precompute) command for details.**
