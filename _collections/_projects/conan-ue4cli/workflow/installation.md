---
title: Installation and setup
pagenum: 1
---

{% include alerts/info.html content="**Important:** make sure you have read the [Key concepts](../read-these-first/concepts) page and familiarised yourself with all of the relevant background material prior to following the steps below." %}


## Manual installation

### Prerequisites and Python package installation

Since conan-ue4cli is a plugin for [ue4cli](../../ue4cli), it shares the same requirements:

- [Python](https://www.python.org/) version 3.5 or newer
- Unreal Engine 4.19.0 or newer

To install ue4cli and conan-ue4cli, run the following command:

{% highlight bash %}
# This may need to be prefixed with sudo under Linux and macOS
pip3 install ue4cli conan-ue4cli
{% endhighlight %}

This will install both ue4cli and conan-ue4cli, along with their dependencies, including the [Conan C++ package management system](https://conan.io/).

### Generating Conan profiles and wrapper packages

In order to compile code that is compatible with a given version of the Unreal Engine, [UE4 Conan profiles](../read-these-first/concepts#ue4-conan-profiles) and [Wrapper packages](../read-these-first/concepts#wrapper-packages) must first be generated for that Engine version. Rather than requesting the path to the Unreal Engine from the user, conan-ue4cli uses the Engine installation that ue4cli is [currently configured to act as an interface for](../../ue4cli/overview/introduction-to-ue4cli#unreal-engine-location-auto-detection).

There is one important factor that must be taken into consideration when performing profile and wrapper package generation. [Installed Builds](https://docs.unrealengine.com/en-US/Programming/Deployment/UsinganInstalledBuild/index.html) of the Unreal Engine (such as those distributed via the Epic Games Launcher) do not contain the full set of files for all of the the Unreal Engine's [bundled third-party libraries](../read-these-first/concepts#unreal-bundled-libraries), whereas Engine installations that have been built from source do include all of the required files. As a result, the workflow for profile and wrapper package generation varies depending on whether you are working with a source build of the Unreal Engine or an Installed Build:

- **If you are using a source build of the Unreal Engine** then you simply need to ensure `ue4cli` is configured to act as an interface to your Engine installation prior to running the [ue4 conan generate](../commands/generate) command:
  
  {% highlight bash %}
# Point ue4cli to your source build of the Unreal Engine
ue4 setroot /path/to/UnrealEngine

# Generate UE4 Conan profiles and wrapper packages for the Engine installation
ue4 conan generate
{% endhighlight %}

- **If you are using an Installed Build of the Unreal Engine** then you will need to download the source code for the relevant Engine version and use the source tree for generating profiles and wrapper packages. Fortunately, it is not necessary to build the Engine from this source tree, merely to run the `Setup.bat`/`Setup.sh` and `GenerateProjectFiles.bat`/`GenerateProjectFiles.sh` scripts to download the required third-party dependency files and build [UnrealBuildTool (UBT)](../read-these-first/concepts#ubt) so it can be queried for information. After profile and wrapper package generation is complete then ue4cli can be configured to act as an interface to the Installed Build you normally use:
  
  {% highlight bash %}
# Clone the source code for the relevant version of the Unreal Engine if you don't already have a local copy
# (Modify the arguments as appropriate if using a custom Engine version, or skip this if unneeded or using Perforce)
git clone --depth=1 -b 4.24.3-release "https://github.com/EpicGames/UnrealEngine.git" /path/to/UnrealEngine

# Download the required third-party dependency files and build UBT
cd /path/to/UnrealEngine
./Setup.sh                 # `call Setup.bat` under Windows
./GenerateProjectFiles.sh  # `call GenerateProjectFiles.bat` under Windows

# Point ue4cli to the Unreal Engine source tree
ue4 setroot /path/to/UnrealEngine

# Generate UE4 Conan profiles and wrapper packages for the Engine installation
ue4 conan generate

# Point ue4cli back to the Installed Build regularly used for development
# (If using ue4cli's auto-detection functionality to locate the Installed Build, run `ue4 clearroot` instead)
ue4 setroot /path/to/Installed/Build
{% endhighlight %}

Once profile and wrapper package generation has completed successfully then you are ready to move on to [Creating Conan packages](./creating-packages).


## Installation using a Docker container image

If you have used [ue4-docker](../../ue4-docker) to build the [ue4-full](../../ue4-docker/building-images/available-container-images#ue4-full) container image for your version of the Unreal Engine then you already have a fully-configured installation of conan-ue4cli inside the container image, including generated [UE4 Conan profiles](../read-these-first/concepts#ue4-conan-profiles) and [Wrapper packages](../read-these-first/concepts#wrapper-packages) for the encapsulated Unreal Engine installation. This installation can be used as-is inside Docker containers or alternatively used to bootstrap the process of configuring conan-ue4cli on the host system:

- **If you want to use conan-ue4cli inside a container**, simply start a container using the built [ue4-full](../../ue4-docker/building-images/available-container-images#ue4-full) container image and you will immediately have access to all conan-ue4cli commands.

- **If you want to use the container image to bootstrap the process of configuring conan-ue4cli on the host system**, you can use the [ue4-docker export](../../ue4-docker/commands/export) command from ue4-docker to export generated wrapper packages to the host system and then create Conan profiles to make use of them:
  
  {% highlight bash %}
# Ensure ue4cli and conan-ue4cli are installed on the host system
# (This may need to be prefixed with sudo under Linux and macOS)
pip3 install ue4cli conan-ue4cli

# Point ue4cli to the Unreal Engine installation that you use on the host system
# (Note that this can be a source build or an Installed Build, it doesn't matter when bootstrapping like this)
ue4 setroot /path/to/UnrealEngine

# Export wrapper packages from the ue4-full container image to the host system's Conan cache
# (Replace "4.24.3" here with the tag for your built ue4-full container image)
# (e.g. specifying "4.23.1" will use the image "adamrehn/ue4-full:4.23.1")
ue4-docker export packages "4.24.3" cache

# Generate UE4 Conan profiles for the host system's Engine installation, skipping wrapper package generation
# since you already have all of the wrapper packages in the local Conan cache after exporting them previously
ue4 conan generate --profile-only
{% endhighlight %}

Once you have a container image that you will use, or the host system has been bootstrapped successfully, then you are ready to move on to [Creating Conan packages](./creating-packages).
