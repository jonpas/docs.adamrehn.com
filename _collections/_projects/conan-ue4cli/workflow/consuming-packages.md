---
title: Consuming Conan packages
pagenum: 3
---

{% include alerts/info.html content="**Important:** make sure you have read the [Key concepts](../read-these-first/concepts) page and familiarised yourself with all of the relevant background material prior to following the steps below." %}


## Generating boilerplate modules

Once you have [created and built Conan packages](./creating-packages) for the third-party libraries or frameworks that you want to integrate into the Unreal Engine then you are ready to consume the built binaries from these packages in an Unreal project or plugin. [Boilerplate modules](../read-these-first/concepts#boilerplate-modules) are the mechanism by which conan-ue4cli makes Conan packages available for consumption by the Unreal Engine's build system. Boilerplate modules are simply regular Unreal Engine [External Modules](../read-these-first/concepts#unreal-modules) whose rules files contain logic to retrieve information about their third-party dependencies from Conan instead of directly hardcoding these details in the way that most External Modules in the Unreal Engine source tree do. To simplify the process of creating boilerplate modules, conan-ue4cli provides functionality to automatically generate them for you.

The [ue4 conan boilerplate](../commands/boilerplate) command is used to generate the source code for boilerplate modules, based on the predefined code that is bundled with conan-ue4cli. It is important to note that there are different versions of the boilerplate code for different versions of the Unreal Engine, so make sure you have configured ue4cli to act as an interface to the correct Engine installation prior to performing boilerplate generation, otherwise conan-ue4cli may use the wrong version of the boilerplate code:

{% highlight bash %}
# Point ue4cli to the Unreal Engine installation that you will use to build the generated boilerplate module
ue4 setroot /path/to/UnrealEngine
{% endhighlight %}

The boilerplate generation command accepts one required argument, which specifies the name of the generated module, and an optional argument to specify the output directory if you do not want the generated module to be placed in the current working directory. For example, if you wanted to create a boilerplate module called "MyBoilerplateModule" then you would run the following command:

{% highlight bash %}
ue4 conan boilerplate MyBoilerplateModule
{% endhighlight %}

This will generate a subdirectory called `MyBoilerplateModule` in the current working directory, containing a module rules file called `MyBoilerplateModule.Build.cs` and a file called [conanfile.py](https://docs.conan.io/en/latest/mastering/conanfile_py.html) for specifying the list of Conan packages that your module consumes. The `MyBoilerplateModule` subdirectory can then be placed under the `Source` directory of an Unreal project or plugin.


## Populating the conanfile.py with dependencies

Once you have generated a boilerplate module, the next step is to populate the list of Conan packages that your module will consume. Some of these packages may have been created by other developers and some may have been created by you, but in either case you should have already [built binaries for the packages](./creating-packages#building-packages) prior to consuming them in a boilerplate module. The generated `conanfile.py` contains automatically-generated scaffolding code to minimise the amount of code that needs to be written manually by the user, so you will only need to modify the `requirements()` function. By default the function contains a single `pass` statement (simply because Python forbids empty function bodies), which can be removed and replaced with the list of Conan packages that the boilerplate module depends upon. An example dependency list is shown below:

{% highlight python %}
# (Start of the scaffolding code omitted for brevity)
# ...

def requirements(self):
    
    # List version 0.0.2 of the MediaIPC-ue4 Conan package as a dependency
    self._requireUnreal("MediaIPC-ue4/0.0.2@adamrehn/{}")
    
    # List version 3.6.1 of the protobuf-ue4 Conan package as a dependency
    self._requireUnreal("protobuf-ue4/3.6.1@adamrehn/{}")

{% endhighlight %}

Note that each listed dependency ends with an empty set of curly braces (`{}`) where the channel portion of a [fully-qualified Conan package reference](https://docs.conan.io/en/latest/faq/using.html#faq-recommendation-user-channel) would be. **This placeholder is required** because it allows the scaffolding code to automatically use the appropriate channel value for the version of the Unreal Engine that is being used to build the boilerplate module.

It is worth pointing out that you only need to list your direct dependencies in the `conanfile.py` code. Conan will automatically resolve any indirect dependencies (the dependencies of your dependencies, and the dependencies of those dependencies, etc.) when the boilerplate module is built.


## Building boilerplate modules

As stated previously, boilerplate modules are ordinary Unreal Engine modules that can be placed under the `Source` directory of any Unreal project or plugin. Because they are External Modules rather than C++ modules, they cannot be included in the `Modules` list of a `.uproject` or `.uplugin` file. Instead, they should be included in the list of dependency modules for a regular C++ module that uses functionality from the third-party libraries or frameworks encapsulated by the boilerplate module. For example, if the Unreal project or plugin contains a boilerplate module called "MyBoilerplateModule" and a C++ module called "MyCodeModule" that uses functionality from the third-party libraries in the boilerplate module, then the file `MyCodeModule.Build.cs` should look this:

{% highlight csharp %}
//Copyright notice and `using` statements go here
//...

public MyCodeModule(ReadOnlyTargetRules Target) : base(Target)
{
    //Other code in the constructor of the module rules class goes here
    //...
    
    PublicDependencyModuleNames.AddRange(
        new string[]
        {
            "MyBoilerplateModule",
            
            //Other public dependencies go here
            //...
        }
    );
    
    //Rest of the code in the constructor of the module rules class goes here
    //...
}
{% endhighlight %}

So long as the boilerplate module is listed as a dependency of at least one C++ module in the same Unreal project or plugin then the boilerplate module will get built automatically when the project or plugin is built. This allows you to iterate on your boilerplate module in the same manner as iterating on C++ code modules.

Once you've created and built a boilerplate module then you have all of the necessary knowledge to use conan-ue4cli to integrate third-party libraries or frameworks into Unreal Engine projects and plugins. If all of the developers working on the project or plugin are using conan-ue4cli and you are only distributing packaged Unreal projects to end users then there are no additional steps required. However, if one or more developers working on the project or plugin are not using conan-ue4cli, or you are distributing packaged Unreal plugins for use by other developers (e.g. via the [Unreal Engine Marketplace](https://www.unrealengine.com/marketplace/)) then you will need to follow additional steps for [Distributing projects and plugins](./distribution).
