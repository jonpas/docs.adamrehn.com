---
title:  ue4 uat
blurb: Invokes RunUAT with the specified arguments.
layout: command
pagenum: 1
---

{% highlight bash %}
{{ page.title | escape }} [ARGS]
{% endhighlight %}

**Description:**

{% capture _alert_content %}
Most common use cases that can be performed by RunUAT already have simpler interfaces provided by other ue4cli commands (e.g. [ue4 build](../project-commands/build), [ue4 test](../project-commands/test), and [ue4 package](../project-commands/package).) Invoking RunUAT directly is only required for advanced usage.
{% endcapture %}
{% include alerts/info.html content=_alert_content %}

This command invokes the `RunUAT.sh` shell script under Linux and macOS, and the `RunUAT.bat` batch file under Windows. This script is the command-line interface to the Unreal AutomationTool, which can be used to perform a variety of tasks. Common uses include:

- Building and packaging projects
- Running automation tests
- Running [BuildGraph](https://docs.unrealengine.com/en-us/Programming/BuildTools/BuildGraph) scripts (e.g. to create an [Installed Build](https://docs.unrealengine.com/en-us/Programming/Deployment/UsinganInstalledBuild) of the Engine)

Any arguments that are specified will be passed directly to the RunUAT script, with the following additions:

- If no `-platform=<PLATFORM>` argument is specified, this argument will be injected with the value for the current system platform (e.g. "Linux", "Mac", "Win64", etc.)
- If no `-project=<PROJECT>` argument is specified, ue4cli will look for a `.uproject` file in the current working directory and will inject this argument with the path to the project file as its value.
