---
title:  ue4 clearroot
blurb: Removes any previously-specified Engine root path override.
layout: command
pagenum: 2
---

{% highlight bash %}
{{ page.title | escape }}
{% endhighlight %}

**Description:**

This command removes any previously-stored path override value that had been specified using the [ue4 setroot](./setroot) command.
