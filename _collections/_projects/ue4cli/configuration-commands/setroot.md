---
title:  ue4 setroot
blurb: Sets an Engine root path override that supersedes auto-detection.
layout: command
pagenum: 3
---

{% highlight bash %}
{{ page.title | escape }} <ROOTDIR>
{% endhighlight %}

**Description:**

This command sets and stores an override value for the root path to the version of the Unreal Engine that ue4cli will act as an interface for. While an override value is present, [auto-detection](../overview/introduction-to-ue4cli#unreal-engine-location-auto-detection) will not be performed. To clear the override value, use the [ue4 clearroot](./clearroot) command.
