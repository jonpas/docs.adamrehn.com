---
title:  ue4 clearcache
blurb: Clears any cached data that ue4cli has stored.
layout: command
pagenum: 1
---

{% highlight bash %}
{{ page.title | escape }}
{% endhighlight %}

**Description:**

This command removes the contents of the ue4cli [cache directory](../overview/technical-details#cache-directory).
