---
title:  ue4 clean
blurb: Cleans build artifacts for the Unreal project or plugin.
layout: command
pagenum: 2
---

{% highlight bash %}
{{ page.title | escape }}
{% endhighlight %}

**Description:**

This command cleans any existing build artifacts for the Unreal project or plugin located in the current working directory. (If no `.uproject` or `.uplugin` file can be found in the current directory then ue4cli will emit an error and halt execution.)

Since invoking UnrealBuildTool with the `-clean` flag can sometimes have unintended side-effects when working with source builds of the Engine, this command works directly with the filesystem instead. The following subdirectories are deleted for both Unreal projects and plugins (cleaning a project will automatically clean all plugins nested within that project):

- `Binaries`
- `Intermediate`
