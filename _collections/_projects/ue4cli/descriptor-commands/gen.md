---
title:  ue4 gen
category: Descriptor Commands
blurb: Generates IDE project files for the Unreal project.
layout: command
pagenum: 3
---

{% highlight bash %}
{{ page.title | escape }} [EXTRA ARGS]
{% endhighlight %}

**Description:**

This command generates project files for the Unreal project located in the current working directory. (If no `.uproject` file can be found in the current directory then ue4cli will emit an error and halt execution.)

The exact mechanism utilised to generate project files varies based on the platform and on whether the UE4 installation is a source build or an Installed Build:

- **Source builds and Installed Builds under Linux and macOS** will invoke `GenerateProjectFiles.sh`, which supports command-line arguments. Any additional arguments that are specified will be passed directly to the script.
- **Source builds under Windows** will invoke `GenerateProjectFiles.bat`, which supports command-line arguments. Any additional arguments that are specified will be passed directly to the script.
- **Installed Builds under Windows** will invoke `UnrealVersionSelector.exe`, which does not support any command-line arguments. Any additional arguments that are specified will be ignored.
