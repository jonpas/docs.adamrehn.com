---
title:  ue4 test
blurb: Runs automation tests for the Unreal project.
layout: command
pagenum: 6
---

{% highlight bash %}
{{ page.title | escape }} [--withrhi] [--list] [--all] [--filter FILTER] TEST1 TEST2 TESTN
{% endhighlight %}

**Description:**

{% include alerts/info.html content="Note that because automation test output is buffered in memory by ue4cli while the tests are running, no output will be visible until all of the tests have completed." %}

This command runs [automation tests](https://docs.unrealengine.com/en-us/Programming/Automation) for the Unreal project located in the current working directory. (If no `.uproject` file can be found in the current directory then ue4cli will emit an error and halt execution.)

The behaviour of this command depends on the arguments that are specified:

- If the `--list` flag is specified then ue4cli will simply print the list of available automation tests for the Unreal project and halt execution.

- If the `--all` flag is specified then ue4cli will run all of the available automation tests for the Unreal project. Note that this includes Engine-wide automation tests and can take quite some time to complete.

- If the `--filter` flag is specified then ue4cli will run all of the automation tests for the Unreal project that match the specified filter. Valid filters are:
  - **Engine**
  - **Smoke**
  - **Stress**
  - **Perf**
  - {::nomarkdown}<p>{:/}**Product** (this is the filter you would most likely use for running automation tests that are specific to your project and any plugins that it contains.){::nomarkdown}</p>{:/}

- If none of the above flags are specified then ue4cli will run the automation tests whose names have been explicitly specified.

By default, automation tests will be run with rendering disabled, which allows them to run in environments that do not have access to GPU acceleration. If you need to enable rendering then you can do so by specifying the `--withrhi` flag.
