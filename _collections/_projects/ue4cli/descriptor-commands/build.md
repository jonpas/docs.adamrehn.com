---
title:  ue4 build
blurb: Builds the Editor modules for the Unreal project or plugin.
layout: command
pagenum: 1
---

{% highlight bash %}
{{ page.title | escape }} [CONFIGURATION] [TARGET] [EXTRA UBT ARGS]
{% endhighlight %}

**Description:**

This command builds the Editor modules for the Unreal project or plugin located in the current working directory. (If no `.uproject` or `.uplugin` file can be found in the current directory then ue4cli will emit an error and halt execution.)

The first optional parameter is used to specify the build configuration to build, which can be any of the valid [Unreal Engine build configuration states](https://docs.unrealengine.com/en-us/Programming/Development/BuildConfigurations) that support building the Editor target (or any valid configuration if building a non-Editor target for a project):

- **Debug**
- **DebugGame**
- **Development**

If no build configuration is explicitly specified then the **Development** configuration will be built by default.

When building Unreal projects, a second optional parameter can be used to specify a custom target to build, which can be any of the valid [Unreal Engine build configuration targets](https://docs.unrealengine.com/en-us/Programming/Development/BuildConfigurations):

- **Editor**
- **Client**
- **Server**

If no target is explicitly specified then the **Editor** target will be built by default. **This parameter is ignored when building Unreal plugins.**

**Examples:**

{% highlight bash %}
# Build the Editor modules for a project or plugin using the default Development configuration
ue4 build

# Build the Editor modules for a project or plugin, explicitly using the Development configuration
ue4 build Development

# Build the Editor modules for a project or plugin using the Shipping configuration
ue4 build Shipping

# Build the Editor modules for a project using the Development configuration
ue4 build Development Editor

# Build the Editor modules for a project using the Shipping configuration
ue4 build Shipping Editor

# Build the Client target for a project using the Development configuration
# (This only works if you've defined a Client target .Build.cs file)
ue4 build Development Client

# Build the Server target for a project using the Development configuration
# (This only works if you've defined a Server target .Build.cs file)
ue4 build Development Server
{% endhighlight %}
