---
title:  Technical details
pagenum: 3
---

## Configuration directory location

The location of the filesystem directory used by ue4cli for storing configuration data varies by operating system:

- **Linux:** `~/.config/ue4cli`
- **macOS:** `~/.config/ue4cli`
- **Windows:** `%APPDATA%\ue4cli`


## Cache directory

To speed up processing when running particularly expensive commands, ue4cli maintains a subdirectory within the configuration directory that stores cached data for reuse during repeated invocations of the same command. All data in the cache is keyed based on a hash of the version information for the Unreal Engine installation to which it pertains, which ensures that cached data from one UE4 installation will not interfere with commands that are interacting with a different installation.

The following commands currently store data in the cache directory:

- [All of the library commands](../library-commands) will store and subsequently utilise a cached version of the JSON output that is produced by UnrealBuildTool when it is interrogated about the list of bundled third-party libraries.

The version-specific nature of the cache keys makes switching between multiple Engine installations extremely quick, but has the downside of leaving old cache data on disk when migrating to newer Engine versions. To clean up the disk space that is consumed by the cache directory, run the [ue4 clearcache](../configuration-commands/clearcache) command.


## UnrealBuildTool library interrogation

To retrieve the details of all available third-party libraries bundled in the `Engine/Source/ThirdParty` subdirectory of the UE4 source tree, UnrealBuildTool is run using the following invocation:

{% highlight bash %}
# <BUILDSCRIPT> will be Build.sh under Linux and macOS, and Build.bat under Windows
# <PLATFORM> will be "Linux", "Mac", "Win64", etc.
<BUILDSCRIPT> UE4Editor <PLATFORM> Development -gather -jsonexport=<JSONFILE> -SkipBuild
{% endhighlight %}

This exports the details of the `UE4Editor` target and all of its dependency modules to a JSON file. Ever since [pull request #4074](https://github.com/EpicGames/UnrealEngine/pull/4074) *(GitHub login required)* was merged in Unreal Engine 4.19.0, this JSON data contains all of the information necessary to link against each available third-party library. This information is then made available to external tools via ue4cli's [library-related commands](../library-commands).
