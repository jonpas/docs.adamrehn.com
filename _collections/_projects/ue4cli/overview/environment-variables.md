---
title:  Environment variables
pagenum: 4
---

You can use the following environment variables to control the behaviour of ue4cli:

- `UE4CLI_CONFIG_DIR`: you can use this to override the location where ue4cli stores its configuration data, instead of using [the platform-specific default location](./technical-details#configuration-directory-location).

- `UE4CLI_VERBOSE`: you can set this to any non-empty value to enable verbose output, which will cause ue4cli to print additional details that are useful for debugging, such as the commands that are being invoked whenever child processes are executed.
