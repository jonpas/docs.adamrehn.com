---
title:  ue4 cmakeflags
blurb: Prints the CMake flags for building against the specified libraries.
layout: command
pagenum: 1
---

{% highlight bash %}
{{ page.title | escape }} [--multiline] [--nodefaults] [LIBS]
{% endhighlight %}

**Description:**

This command prints the CMake flags required to build against the specified list of libraries. (To determine the available library names, run the [ue4 libs](./libs) command.) The generated flags include `CMAKE_PREFIX_PATH`, `CMAKE_INCLUDE_PATH`, and `CMAKE_LIBRARY_PATH`.

If the `--multiline` flag is specified then each flag will be printed on a separate line. The default is to print all of the flags on a single line, delimited by spaces.

If the `--nodefaults` flag is specified when running under Linux then the details for building against libc++ will not be included in the output. This flag does nothing under macOS and Windows.
