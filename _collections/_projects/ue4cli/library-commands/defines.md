---
title:  ue4 defines
blurb: Prints the preprocessor definitions for the specified libraries.
layout: command
pagenum: 3
---

{% highlight bash %}
{{ page.title | escape }} [--nodefaults] [LIBS]
{% endhighlight %}

**Description:**

This command prints the preprocessor definitions for the specified list of libraries. (To determine the available library names, run the [ue4 libs](./libs) command.)

If the `--nodefaults` flag is specified when running under Linux then the details for building against libc++ will not be included in the output. This flag does nothing under macOS and Windows.
