---
title:  ue4 libs
blurb: Lists the third-party libraries bundled with the Unreal Engine installation.
layout: command
pagenum: 7
---

{% highlight bash %}
{{ page.title | escape }}
{% endhighlight %}

**Description:**

This command will print a list of the names of all available third-party libraries that are bundled with the Unreal Engine installation that ue4cli is currently acting as an interface to. These names can then be used as arguments to the other library-related commands in order to retrieve further information about any given libraries.
