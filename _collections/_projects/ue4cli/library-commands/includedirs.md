---
title:  ue4 includedirs
blurb: Prints the header include directories for the specified libraries.
layout: command
pagenum: 4
---

{% highlight bash %}
{{ page.title | escape }} [--nodefaults] [LIBS]
{% endhighlight %}

**Description:**

This command prints the paths to the header include directories for the specified list of libraries. (To determine the available library names, run the [ue4 libs](./libs) command.)

If the `--nodefaults` flag is specified when running under Linux then the details for building against libc++ will not be included in the output. This flag does nothing under macOS and Windows.
