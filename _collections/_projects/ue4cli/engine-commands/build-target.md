---
title:  ue4 build-target
blurb: Build the specified target using UBT.
layout: command
pagenum: 1
---

{% highlight bash %}
{{ page.title | escape }} <TARGET> [CONFIGURATION] [EXTRA UBT ARGS]
{% endhighlight %}

**Description:**

This command invokes UBT to directly build arbitrary targets. Unlike the [ue4 build](../descriptor-commands/build) command, these targets are not scoped to a project or plugin descriptor, so this is primarily useful for building Engine tools and programs when working with a source build of the Unreal Engine.

The first parameter specifies the target to build (e.g. `UE4Editor`, `ShaderCompileWorker`, `UnrealFrontend`) and is required.

The second parameter optionally specifies the build configuration to build, which can be any of the valid [Unreal Engine build configuration states](https://docs.unrealengine.com/en-us/Programming/Development/BuildConfigurations):

- **Debug**
- **DebugGame**
- **Development**
- **Shipping**
- **Test**

If no build configuration is explicitly specified then the **Development** configuration will be built by default.

**Examples:**

{% highlight bash %}
# Build the Unreal Editor using the default Development configuration
# (Works for source builds of the Engine and is a no-op in Installed Builds)
ue4 build-target UE4Editor

# Build the Unreal Editor, explicitly using the Development configuration
# (Works for source builds of the Engine and is a no-op in Installed Builds)
ue4 build-target UE4Editor Development

# Build the ShaderCompileWorker tool using the Shipping configuration
# (Works for source builds of the Engine and fails in Installed Builds because the target doesn't exist)
ue4 build-target ShaderCompileWorker Shipping
{% endhighlight %}
